package com.hfad.bitsandpizzas

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.hfad.bitsandpizzas.databinding.FragmentOrderBinding

class OrderFragment : Fragment() {
    private var _binding: FragmentOrderBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater:           LayoutInflater,
                              container:          ViewGroup?,
                              savedInstanceState: Bundle?)
        : View?
    {
        // Inflate the layout for this fragment
        _binding = FragmentOrderBinding.inflate(inflater, container, false)
        val view = binding.root
        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)

        binding.fab.setOnClickListener {
            val pizzaType = binding.pizzaGroup.checkedRadioButtonId

            if (pizzaType == -1) {
                val text = "You must select a pizza type."
                Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()

            } else {
                var text = "Order confirmed: " +
                    when (pizzaType) {
                        R.id.radio_diavolo -> "Diavolo pizza"
                        R.id.radio_funghi  -> "Funghi pizza"
                        R.id.radio_pan     -> "Pan pizza"
                        else               -> "An unknown pizza! TODO: error?"
                    }

                text += if (binding.parmesan.isChecked)  ", extra parmesan" else ""
                text += if (binding.pepperoni.isChecked) ", pepperoni" else ""
                text += if (binding.cheesy.isChecked)    ", cheese" else ", no cheese"
                text += if (binding.pineapple.isChecked) ", pineapple" else ""
                text += if (binding.kiwi.isChecked)      ", kiwi" else ""
                text += if (binding.ketchup.isChecked)   ", ketchup as sauce" else ""

                Snackbar.make(binding.fab, text, Snackbar.LENGTH_SHORT)
                    .show()
            }
        }

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}